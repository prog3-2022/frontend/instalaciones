# instalaciones



## Además de las herramientas ya instaladas para el backend

- [Git](https://git-scm.com/)
- [Postman](https://www.postman.com/)

## Instalaciones necesarias para frontend

- [Node](https://nodejs.org/es/)
- [Google Chrome](https://www.google.com/chrome/)
- [WebStorm](https://www.jetbrains.com/webstorm/promo/?source=google&medium=cpc&campaign=9641686239&term=webstorm&gclid=CjwKCAjw6fyXBhBgEiwAhhiZso5HYTy6CYyxx7e7yZ9aLFctws8BpxbVOsQVOcoVDz6qLvn-vZsXvxoC_QgQAvD_BwE)
- [VSCode - Visual Studio Code](https://code.visualstudio.com/)

## Links útiles

- [w3schools](https://www.w3schools.com/)
- [Bootstrap](https://getbootstrap.com/)
- [Tailwind](https://tailwindcss.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [Webpack | TypeScript](https://webpack.js.org/guides/typescript/)
- [Angular](http://angular.dev/)
- [Angular Material](https://material.angular.io/)
- [PrimeNg](https://primeng.org/)
- [PrimeFlex](https://primeflex.org/)

## Extensiones de VSCode

- [TypeScript Importer - optional](https://marketplace.visualstudio.com/items?itemName=pmneo.tsimporter)
- [Editor Config for VSCode](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)
- [Terminal](https://marketplace.visualstudio.com/items?itemName=formulahendry.terminal)
- [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)
- [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
- [Angular Snippets](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2)
- [Angular Schematics](https://marketplace.visualstudio.com/items?itemName=cyrilletuzi.angular-schematics)
- [Angular 2 Inline](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)
- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [Error Lens](https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens)
- [Paste JSON as Code](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype)

